package com.example.maxim.lestmakeamericagreatagain;


import android.annotation.TargetApi;
import android.content.Context;
import android.hardware.camera2.*;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Switch flashSwith = (Switch)findViewById(R.id.switchFlash);
        flashSwith.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                String message = new String("Switch state=");
                message += ""+b;
                Log.v("Switch state=",""+b);
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                FlashState(b);

            }

            @TargetApi(Build.VERSION_CODES.M)
            private void FlashState(boolean b) {
               /* try{

                    CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
                    String[] list = manager.getCameraIdList();
                    CameraCharacteristics cameraCharacteristics = manager.getCameraCharacteristics(list[0]);

                    Log.v("TAG",list[0]);
                    manager.setTorchMode(list[0], b);
                }
                catch (CameraAccessException cae){
                    Log.v("TAG",cae.getMessage());
                    cae.printStackTrace();
                }*/


                try {
                    CameraManager camManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
                    String cameraId = camManager.getCameraIdList()[0];
                    camManager.setTorchMode(cameraId, b);
                }
                catch (CameraAccessException cae)
                {
                    Log.v("TAG", cae.getMessage());
                }


            }

        });
    }

}
