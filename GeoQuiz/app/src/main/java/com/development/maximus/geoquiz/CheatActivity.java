package com.development.maximus.geoquiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CheatActivity extends AppCompatActivity {

    public static final String EXTRA_ANSWER_IS_TRUE = "com.development.maximus.geoquiz.answer_is_true";
    public static final String EXTRA_ANSWER_SHOWN = "com.development.maximus.geoquiz.answer_shown";
    private static final String TAG = "CheatActivity";
    private static final String KEY_CHEAT = "cheat";
    private boolean mAnswerIsTrue;
    private boolean mIsAnswerShown = false;
    private TextView mAnswerTextView;
    private Button mShowAnswer;


    private void setAnswerShownResult()
    {
        Intent data = new Intent();
        data.putExtra(EXTRA_ANSWER_SHOWN, mIsAnswerShown);
        setResult(RESULT_OK, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheat);
        Log.d(TAG, "CheatActivity Created");
        mAnswerIsTrue = this.getIntent().getBooleanExtra(EXTRA_ANSWER_IS_TRUE,false);
        mIsAnswerShown = false;
        if (savedInstanceState!= null)
        {
            mIsAnswerShown = savedInstanceState.getBoolean(KEY_CHEAT, false);
        }
        setAnswerShownResult();
        mAnswerTextView = (TextView)findViewById(R.id.answerTextView);
        mShowAnswer = (Button)findViewById(R.id.showAnswerButton);
        mShowAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Show Answer Clicked");
                if (mAnswerIsTrue){

                    mAnswerTextView.setText(R.string.true_button);
                }else
                {
                    mAnswerTextView.setText(R.string.false_button);
                }
                mIsAnswerShown = true;
                setAnswerShownResult();
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle data)
    {
        data.putBoolean(KEY_CHEAT,mIsAnswerShown);
    }
}
