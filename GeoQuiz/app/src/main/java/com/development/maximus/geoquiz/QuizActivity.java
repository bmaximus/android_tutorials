package com.development.maximus.geoquiz;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class QuizActivity extends AppCompatActivity {

    private Button mTrueButton;
    private Button mFalseButton;
    private Button mNextButton;
    private Button mPreviousButton;
    private Button mCheatButton;
    private TextView mQuestionTextView;
    private int mCurrentIndex = 0;
    private boolean mIsCheater;
    private static final String KEY_INDEX = "index";
    private static final String KEY_CHEAT = "cheat";
    private static final String TAG = "QuizActivity";

    private TrueFalse[] mQuestionBank = new TrueFalse[]{
            new TrueFalse(R.string.question_oceans,true),
            new TrueFalse(R.string.question_mideast,false),
            new TrueFalse(R.string.question_africa,false),
            new TrueFalse(R.string.question_americas,true),
            new TrueFalse(R.string.question_asia,true),
    };
    private void NextQuestion()
    {
        IncreaseIndex();
        UpdateQuestion();
    }
    private void IncreaseIndex()
    {
        mCurrentIndex = (mCurrentIndex + 1) % mQuestionBank.length;
    }
    private void DecreaseIndex()
    {
        if (mCurrentIndex == 0)
        {
            mCurrentIndex = mQuestionBank.length-1;
        }
        else
        {
            mCurrentIndex = ((mCurrentIndex -1) % mQuestionBank.length);
        }
        Log.d(TAG,""+mCurrentIndex);
    }
    private void UpdateQuestion()
    {
        int question = mQuestionBank[mCurrentIndex].getmQuestion();
        mQuestionTextView.setText(question);
    }
    private void CheckAnswer(boolean userPressed)
    {
        boolean isTrueQuestion = mQuestionBank[mCurrentIndex].ismTrueQuestion();
        int messageResultId = 0;
        if (mIsCheater) {
            messageResultId = R.string.judgment_toast;
        }
        else
        {
            if (userPressed==isTrueQuestion){
                messageResultId = R.string.correct_toast;
            }else
            {
                messageResultId = R.string.incorrect_toast;
            }
        }
        Toast.makeText(this, messageResultId, Toast.LENGTH_SHORT).show();
        NextQuestion();
        mIsCheater = false;
    }


    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"onCreate(Bundle) called");
        setContentView(R.layout.activity_quiz);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
        {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setSubtitle("Bodies of Water");
        }
        if (savedInstanceState!=null){
            mCurrentIndex = savedInstanceState.getInt(KEY_INDEX,0);
            mIsCheater = savedInstanceState.getBoolean(KEY_CHEAT,false);
        }

        mQuestionTextView =(TextView) findViewById(R.id.question_text_view);
        mIsCheater = false;
        UpdateQuestion();

        mTrueButton = (Button)findViewById(R.id.true_button);
        mFalseButton = (Button)findViewById(R.id.false_button);
        mNextButton = (Button)findViewById(R.id.next_button);
        mNextButton = (Button)findViewById(R.id.next_button);
        mPreviousButton = (Button)findViewById(R.id.previous_button);
        mCheatButton = (Button)findViewById(R.id.cheat_button);



        mTrueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckAnswer(true);
                    Log.d(TAG,"True Clicked");

            }
        });

        mFalseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckAnswer(false);
                Log.d(TAG,"False Clicked");
            }
        });

        mNextButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                IncreaseIndex();
                UpdateQuestion();
                Log.d(TAG,("Next Clicked, Current Index:"+mCurrentIndex));
            }
        });
        mPreviousButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                DecreaseIndex();
                UpdateQuestion();
                Log.d(TAG,("Previous Clicked, Current Index:"+mCurrentIndex));
            }
        });
        mQuestionTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IncreaseIndex();
                UpdateQuestion();
                Log.d(TAG,("TextView Clicked, Current Index:"+mCurrentIndex));
            }
        });
        mCheatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(QuizActivity.this, CheatActivity.class);
                boolean realAnswer = mQuestionBank[mCurrentIndex].ismTrueQuestion();
                intent.putExtra(CheatActivity.EXTRA_ANSWER_IS_TRUE,realAnswer);
                //startActivity(intent);
                startActivityForResult(intent,0);
                Log.d(TAG, "mCheatButtonClicked");
            }
        });

    }
    @Override
    protected void onStart(){
        super.onStart();
        Log.d(TAG,"onStart() called");

    }
    @Override
    protected void onPause(){
        super.onPause();
        Log.d(TAG,"onPause() called");
    }
    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume() called");
    }
    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop() called");
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy() called");
    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);

        Log.d(TAG,"onSaveInstanceState");
        savedInstanceState.putInt(KEY_INDEX,mCurrentIndex);
        savedInstanceState.putBoolean(KEY_CHEAT,mIsCheater);

    }
    @Override
    public void onActivityResult(int requestedCode, int resultCode, Intent data)
    {
       if (data==null)
       {
           return;
       }
        mIsCheater = data.getBooleanExtra(CheatActivity.EXTRA_ANSWER_SHOWN,false);
    }


}
